import React, {Component} from 'react';
import logo from '../logo.svg';

export default class Header extends Component {

	render() {

		return (
			<header className="row">
				<div className="col col-6 margin-md">
					<img src={logo} className="logo" alt="FFMedia AB"/>
					<h1 className="h1 margin-df">Hej!</h1>
					<h2 className="h3 margin-sm">Tack för att du valde att anlita Fast Forward Media.</h2>
					<p>{ this.props.invoiceDescription}</p>
				</div>
				<div className="col col-6">
					<h1 className="h1 margin-sm">{this.props.invoiceTitle}</h1>
					<h2 className="h2 margin-df">Fakturanummer: {this.props.invoiceNumber}</h2>
					<div className="row margin-df">
						<div className="col col-4">
							<h3 className="h3">Utskriftsdatum:</h3>
						</div>
						<div className="col col-8">
							<p>{this.props.invoiceDate}</p>
						</div>
					</div>
					<div className="row margin-df">
						<div className="col col-4">
							<h3 className="h3">Er referens:</h3>
						</div>
						<div className="col col-8">
							<p style={{whiteSpace: 'pre-line'}}>{this.props.yourRef}</p>
						</div>
					</div>
					<div className="row margin-df">
						<div className="col col-4">
							<h3 className="h3">Vår referens:</h3>
						</div>
						<div className="col col-8">
							<p>{this.props.ourRef}</p>
						</div>
					</div>
					<div className="row">
						<div className="col col-4">
							<h4 className="h3 margin-sm">Adress:</h4>
						</div>
						<div className="col col-8">
							<p><span>FFMedia AB</span> c/o Tatsumi Suzuki</p>
							<p>Bergsunds Strand 13, 7tr</p>
							<p>117 38 Stockholm</p>
						</div>
					</div>
				</div>
			</header>
		);
	}
}
