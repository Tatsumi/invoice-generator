import React, {Component} from 'react';

export default class Footer extends Component {

	render() {
		return (
			<footer>
				<div className="row">
					<div className="col col-3 margin-sm">
						<p className="h3">E-post:</p>
						<p>hej@fastforwardmedia.se</p>
					</div>
					<div className="col col-3 margin-sm">
						<p className="h3">Telefon:</p>
						<p>+46 (0) 735-00 30 34</p>
					</div>
					<div className="col col-3 margin-sm">
						<p className="h3">Organisationsnummer:</p>
						<p>559017-3802</p>
					</div>
					<div className="col col-3 margin-sm">
						<p className="h3">Säte:</p>
						<p>Stockholm</p>
					</div>
				</div>
			</footer>
		);
	}
}
