import { combineReducers } from 'redux';

import {
	companiesFetched
} from './companies';

export default combineReducers({
	companiesFetched
});
