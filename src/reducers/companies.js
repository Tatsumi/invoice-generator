export function companiesFetched(state = {}, action) {

	switch(action.type) {
		case 'FETCH_COMPANIES_FULFILLED':
			return { ...state, companies: action.payload };
		case 'FETCH_COMPANIES_REJECTED':
			return { ...state, companies: action.error };
		default:
			return state;
	}

	return state;
}
